package test;


import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase{
	/**
	 * Clase a probar
	 */
	private Stack<String> stack;
	/**
	 * Escenario a trabajar
	 */
	public void setup(){
		stack=new Stack();
		stack.push("a");
		stack.push("b");
		stack.push("c");
	}
	/**
	 * Prueba 1: verifica eliminar elementos y contar la cantidad de ellos
	 */
	public void test1(){
		setup();
		assertEquals("El numero de elementos debe ser 3",stack.size(),3);
		stack.pop();
		assertEquals("El numero de elementos debe ser 3",stack.size(),2);
		stack.pop();
		assertEquals("El numero de elementos debe ser 2",stack.size(),1);
	}

}
