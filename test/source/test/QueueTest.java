package test;


import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase{
	/**
	 * Clase a probar
	 */
	private Queue<String> queue;
	/**
	 * Escenario a trabajar
	 */
	public void setup(){
		queue=new Queue();
		queue.enqueue("a");
		queue.enqueue("b");
		queue.enqueue("c");
	}
	/**
	 * Prueba 1: verifica eliminar elementos y contar la cantidad de ellos
	 */
	public void test1(){
		setup();
		assertEquals("El numero de elementos debe ser 3",queue.size(),3);
		queue.dequeue();
		assertEquals("El numero de elementos debe ser 3",queue.size(),2);
		queue.dequeue();
		assertEquals("El numero de elementos debe ser 2",queue.size(),1);
	}

}
