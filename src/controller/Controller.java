package controller;

import api.ITaxiTripsManager;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId */
	public static void loadServices( String taxiId ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		//taxiId="d5c07ff3802aceed5373a10ec72eef292113d324257906f4d51eb193f83e56f6d53faf6285a88a7e224b1807fc1e65bbc3795fe0dc2037c18694579d0c88d119";
		manager.loadServices( serviceFile, taxiId );
	}
		
	public static int [] servicesInInverseOrder() {
		return manager.servicesInInverseOrder();
	}
	
	public static int [] servicesInOrder() {
		return manager.servicesInOrder();
	}
}
