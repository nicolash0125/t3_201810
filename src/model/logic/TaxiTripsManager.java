package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	Stack<Service> serviceStack;
	Queue<Service> serviceQueue;
	
	public void loadServices(String serviceFile, String taxiId) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		JsonParser parser = new JsonParser();
		serviceStack = new Stack<Service>();
		serviceQueue= new Queue<Service>();
		JsonArray array;
		String taxi="";
		try {
			array = (JsonArray) parser.parse(new FileReader(serviceFile));
			
			for (int i = 0; i < array.size() ; i++) {
				JsonObject object=(JsonObject) array.get(i);
				if(object.get("taxi_id")!=null)
					taxi=object.get("taxi_id").getAsString();
				
				if(taxiId.compareTo(taxi)==0)
					cargarInformacion(object);
					
					//System.out.println(i);
			}
			//System.out.println("hola"+serviceQueue.size());
			//System.out.println(serviceStack.size());
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		resultado[1]=0;
		ArrayList<Service>serv=new ArrayList<Service>();
	
		Service s1=serviceStack.pop();
		serv.add(s1);
		Service s2=serviceStack.pop();
		serv.add(s2);
		while(s1!=null&&s2!=null)
		{
			//System.out.print("S1: "+s1.gettrip_start_timestamp()+"   S2: "+s2.gettrip_start_timestamp());
			//System.out.println();
			if(s1.compareTo(s2)<0){
				//System.out.println("CUENTO");
				resultado[1]++;
				s2=serviceStack.pop();
				if(s2!=null)serv.add(s2);
			}
			else{
				s1=s2;
				s2=serviceStack.pop();
				if(s2!=null)serv.add(s2);
			}
			
		}
		
		for(int i=serv.size();i>0;i--){
			serviceStack.push(serv.get(i-1));
		}
		resultado[0]=serviceStack.size()-resultado[1];
	
		return resultado;
	}

	@Override
	public int [] servicesInOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInOrder");
		int [] resultado = new int[2];
		
		resultado[1]=0;
		ArrayList<Service>serv=new ArrayList<Service>();
	
		Service s1=serviceQueue.dequeue();
		serv.add(s1);
		Service s2=serviceQueue.dequeue();
		serv.add(s2);
		while(s1!=null&&s2!=null)
		{
			//System.out.print("S1: "+s1.gettrip_start_timestamp()+"   S2: "+s2.gettrip_start_timestamp());
			//System.out.println();
			if(s1.compareTo(s2)>0){
				//System.out.println("CUENTO");
				resultado[1]++;
				s2=serviceQueue.dequeue();
				if(s2!=null)serv.add(s2);
			}
			else{
				s1=s2;
				s2=serviceQueue.dequeue();
				if(s2!=null)serv.add(s2);
			}
			
		}
		
		for(int i=0;i<serv.size();i++){
			serviceQueue.enqueue(serv.get(i));
		}
		resultado[0]=serviceQueue.size()-resultado[1];
	
		
		return resultado;
	}
	
	
	
	public void cargarInformacion(JsonObject object){
		
		String company="";
		if (object.get("company")!=null)
			company=object.get("company").getAsString();
		
		//Trip 
		String trip_id=" ";
		if (object.get("trip_id")!=null)
			trip_id=object.get("trip_id").getAsString();
		String taxi_id=" ";
		if (object.get("taxi_id")!=null)
			taxi_id=object.get("taxi_id").getAsString();
		int seconds=0;
		if (object.get("trip_seconds")!=null)
			seconds=object.get("trip_seconds").getAsInt();
		Double miles=(double) 0;
		if (object.get("trip_miles")!=null)
			miles=object.get("trip_miles").getAsDouble();
		
		Double total_cost=(double) 0;
		if (object.get("trip_total")!=null)
			total_cost=object.get("trip_total").getAsDouble();
		
		int dropoff_community_area=(int) 0;
		if (object.get("dropoff_community_area")!=null)
			dropoff_community_area=object.get("dropoff_community_area").getAsInt();
		long trip_start_timestamp=0;
		if(object.get("trip_start_timestamp")!=null){
			trip_start_timestamp=traducirFecha(object.get("trip_start_timestamp").getAsString());
		}
			
		
		//Taxi t=new  Taxi(taxi_id, company);
		//System.out.println(t);
		
		
		
		
		Service s=new Service(
				trip_id
				, 
				taxi_id
				,
				seconds
				,
				miles
				,
				total_cost
				,
				dropoff_community_area
				,
				trip_start_timestamp);
		serviceQueue.enqueue(s);
		serviceStack.push(s);
	
	}

	/**
	 * Convierte una fecha a milisegundos desde el 1 de enero de 1970
	 * @param asString
	 * @return long milisegundos 
	 */
	public long traducirFecha(String asString) {
		//Ejemplo fecha
		//2017-02-01T09:00:00.000
		//System.out.println(asString);
		//0T1
		String [] div=asString.split("T");
		//0-1-2
		String [] date=div[0].split("-");
		//0:1:2
		String [] time=div[1].split(":");
		//0.1
		String [] sec=time[2].split("[.]");
		
		int year=Integer.parseInt(date[0]);
		//System.out.println(year);
		year-=1900;
		int month=Integer.parseInt(date[1]);
		//System.out.println(month);
		month-=1;
		int day=Integer.parseInt(date[2]);
		//System.out.println(day);
		int hour =Integer.parseInt(time[0]);
		//System.out.println(hour+"hora");
		int min=Integer.parseInt(time[1]);
		//System.out.println(min);
		
		//System.out.println(time[2]);
		int seconds=Integer.parseInt(sec[0]);
		//System.out.println(seconds);
		
		long msecons=Long.parseLong(sec[1]);
		//System.out.println(msecons);
		
		
		Date d=new Date(year, month, day, hour, min, seconds);
		
		msecons=msecons+d.getTime();
		
		//d.setTime(msecons);
		//System.out.println(d.toGMTString());
		
		return msecons;
	}


}
