package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxi_id;
	private String company;

	
	
	public Taxi(String taxi_id, String company){
		this.taxi_id=taxi_id;
		this.company=company;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		if(taxi_id.equals(o.getTaxiId()))
			return 0;
		else return -1;
	}	
	public String toString(){
		return "Taxi id:"+taxi_id+" Compania: "+company;
	}
}
