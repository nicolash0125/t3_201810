package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	
	private String trip_id;
	private String taxi_id;
	private int seconds;
	private double miles;
	private double total_cost;
	private int dropoff_community_area;
	private long trip_start_timestamp;
	
	public Service(String trip_id, String taxi_id, int seconds, double miles, double total_cost, int dropoff_community_area, long trip_start_timestamp){
		this.trip_id=trip_id;
		this.taxi_id=taxi_id;
		this.seconds=seconds;
		this.miles=miles;
		this.total_cost=total_cost;
		this.dropoff_community_area=dropoff_community_area;
		this.trip_start_timestamp=trip_start_timestamp;
	}
	
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}
	public long gettrip_start_timestamp(){
		return trip_start_timestamp;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		if(trip_start_timestamp==o.gettrip_start_timestamp())
			return 0;
		if(trip_start_timestamp<o.gettrip_start_timestamp())
			return -1;
		return 1;
	}
	public int getDropoff_community_area(){
		return dropoff_community_area;
	}
	public String toString(){
		return "Servicio "+trip_id+" Taxi:"+taxi_id+"Zona "+ dropoff_community_area;
	}
}
